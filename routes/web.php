<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Auth\LoginController;
use App\Models\Event;


// Rutas de seguridad
Route::get('login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');


// Rutas de Dashboard
Route::match(['get', 'post'], '/', function(){ return view( 'pages.dashboard.dashboard'); })->middleware('auth');


// rutas de vistas
Route::view('/pages/tickets', 'pages.tickets.index');
Route::view('/pages/events', 'pages.events.index');
Route::view('/pages/sales', 'pages.sales.index');


// Rutas Ventas
Route::resource('sales', App\Http\Controllers\SaleController::class)->middleware('auth');

// Rutas Tickets
Route::resource('tickets', App\Http\Controllers\TicketController::class)->middleware('auth');

// Rutas de eventos
Route::resource('events', App\Http\Controllers\EventController::class)->middleware('auth');
Route::get('getEvents', [App\Http\Controllers\EventController::class, 'getEvents'])->middleware('auth');
