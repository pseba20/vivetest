<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('order')->nullable();
            $table->integer('count')->nullable();;
            $table->longText('photo')->nullable();;
            $table->string('name')->nullable();;
            $table->string('surname')->nullable();;
            $table->string('NIF')->nullable();;
            $table->bigInteger('event_id')->unsigned()->nullable()->default(null);
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
