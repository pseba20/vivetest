<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $newUser = User::create([
			'name' => 'Administrador',
			'role' => 'Administrador',
			'email' => 'administrador@vivetix.com',
			'password' => bcrypt('123456'),
		]);


        $newUser = User::create([
			'name' => 'Operador',
			'role' => 'Operador',
			'email' => 'operador@vivetix.com',
			'password' => bcrypt('123456'),
		]);
    }
}
