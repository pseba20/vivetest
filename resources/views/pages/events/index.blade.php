@extends('layouts.backend')

@section('css')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables-bs5/css/dataTables.bootstrap5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables-buttons-bs5/css/buttons.bootstrap5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables-responsive-bs5/css/responsive.bootstrap5.min.css') }}">
@endsection

@section('js')
    <!-- jQuery (required for DataTables plugin) -->
    <script src="{{ asset('js/lib/jquery.min.js') }}"></script>

    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons/buttons.html5.min.js') }}"></script>

    <!-- Page JS Code -->
    @vite(['resources/js/pages/events/events.js'])
@endsection

@section('content')
    <!-- Page Content -->
    <div class="content">



        <!-- Heading -->
        <div class="my-5 text-center">
            <h1 class="h3 fw-extrabold mb-1">
                Administrar Eventos
            </h1>

        </div>
        <!-- END Heading -->

        <!-- Info -->
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="block block-rounded">
                </div>
            </div>
        </div>
        <!-- END Info -->



        <!-- Dynamic Table with Export Buttons -->
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Eventos disponibles <small>lista</small>

                </h3>
                <span class="block-options pull-end">
                    <a href="javascript:void(0)" class="btn btn-rounded btn-noborder btn-success" id="btn_newEvent">
                        <i class="fa fa-plus-circle"></i> Crear
                    </a>
                </span>
            </div>


            <div class="block-content">
                <table id="eventsDT"
                    class="table table-bordered table-vcenter table-responsive-sm js-dataTable-full dataTable no-footer"
                    width="100%">
                    <thead class="thead-light">
                        <tr>
                            <th>id</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th>Cantidad</th>
                            <th>Descripcion</th>
                            <th style="width: 15%;">Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->

    <!-- Inica Modal agregar evento -->

    <div class="modal modal-lg fade event" id="staticBackdropEvent" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropEventLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropEventLabel">Crear evento</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal " id="form_event">
                        <input type="hidden" name="eventId" id="eventId" value="">

                        <div class="modal-body">
                            @csrf
                            <div class="row mt-2">
                                <div class="col-md">
                                    <div class="form-group ">
                                        <label form="name">Nombre</label>
                                        <input type="text" class="form-control" name="name" id="name" required
                                            minlength="3" maxlength="35">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">

                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Cantidad de tickets disponibles</label>
                                        <input type="number" min="1" class="form-control" name="count"
                                            id="count" required>

                                    </div>
                                </div>

                                <div class="col-md">
                                    <div class="form-group ">
                                        <label for="description">Fecha </label>
                                        <input type="date" class="form-control" name="date" id="date" required
                                            minlength="10">
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                                    </div>
                                </div>


                            </div>

                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button id="btn_addEvent" type="button" class="btn btn-primary">Crear</button>
            </div>
        </div>
    </div>
    </div>

    <!-- fin Modal agregar evento -->
@endsection
