@extends('layouts.backend')

@section('content')
  <!-- Page Content -->
  <div class="content">
    <div class="row items-push">
      <div class="col-md-12 col-xl-12">
        <div class="block block-rounded h-100 mb-0">
          <div class="block-header block-header-default">
            <h3 class="block-title">
              Welcome to your app
            </h3>
          </div>
          <div class="block-content text-muted">
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, nobis est. Amet eos sit illum nostrum sed perspiciatis ratione tempore laboriosam sapiente alias. Fugit, amet. Blanditiis, sapiente ratione? Ipsum, saepe.
            </p>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, at. Voluptates ad consequatur ea eveniet nobis ipsa, in inventore, sunt eum consectetur possimus. Aliquid reiciendis accusantium suscipit, exercitationem iure esse.
            </p>
            <p class="fw-semibold">
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Debitis modi, ad at iste quibusdam expedita placeat tempore sed architecto laboriosam consequatur? Nulla doloribus voluptatem assumenda reiciendis et! Obcaecati, repellat? Quo!
            </p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- END Page Content -->
@endsection
