@extends('layouts.backend')

@section('css')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables-bs5/css/dataTables.bootstrap5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables-buttons-bs5/css/buttons.bootstrap5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables-responsive-bs5/css/responsive.bootstrap5.min.css') }}">
@endsection

@section('js')
    <!-- jQuery (required for DataTables plugin) -->
    <script src="{{ asset('js/lib/jquery.min.js') }}"></script>

    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons-pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables-buttons/buttons.html5.min.js') }}"></script>

    <!-- Page JS Code -->
    @vite(['resources/js/pages/tickets/tickets.js'])
@endsection

@section('content')
    <!-- Page Content -->
    <div class="content">



        <!-- Heading -->
        <div class="my-5 text-center">
            <h1 class="h3 fw-extrabold mb-1">
                Venta de Tickets
            </h1>

        </div>
        <!-- END Heading -->

        <!-- Info -->
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="block block-rounded">
                </div>
            </div>
        </div>
        <!-- END Info -->



        <!-- Dynamic Table with Export Buttons -->
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Tickets vendidos <small>lista</small>

                </h3>
                <span class="block-options pull-end">
                    <a href="javascript:void(0)" class="btn btn-rounded btn-noborder btn-success" id="btn_newTicket">
                        <i class="fa fa-plus-circle"></i> Vender
                    </a>
                </span>
            </div>


            <div class="block-content">
                <table id="ticketsDT"
                    class="table table-bordered table-vcenter table-responsive-sm js-dataTable-full dataTable no-footer"
                    width="100%">
                    <thead class="thead-light">
                        <tr>
                            <th>id</th>
                            <th>Orden</th>
                            <th>Evento</th>
                            <th>Cantidad</th>
                            {{-- <th>Comprobante de pago</th>
                            <th>Datos</th> --}}
                            <th style="width: 15%;">Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->

    <!-- Inica Modal agregar ticket -->

    <div class="modal fade ticket" id="staticBackdropTicket" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropTicketLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropTicketLabel">Vender Tickets</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form class="form-horizontal " id="form_ticket">
                    <input type="hidden" name="ticketId" id="ticketId" value="">

                    <div class="modal-body">
                        @csrf
                        <div class="row mt-2">
                            <div class="col-md">
                                <div class="form-group ">
                                    <label form="name">Seleccione el Evento</label>
                                    <select class="form-control select2" name="event_id" id="event_id" style="width: 100%">
                                        <option value="0">Ninguno</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md">
                                <div class="form-group ">
                                    <label form="name">Nombre</label>
                                    <input type="text" class="form-control" name="name" id="name" required
                                        minlength="3" maxlength="35">
                                </div>
                            </div>

                            <div class="col-md">
                                <div class="form-group ">
                                    <label form="name">Apellido</label>
                                    <input type="text" class="form-control" name="surname" id="surname" required
                                        minlength="3" maxlength="35">
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">

                            <div class="col-md">
                                <div class="form-group">
                                    <label>NIF/DNI</label>
                                    <input type="text" min="1" class="form-control" name="nif" id="nif" required>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">

                            <div class="col-md">
                                <div class="form-group">
                                    <label>Cantidad de tickets</label>
                                    <input type="number" min="1" class="form-control" name="count" id="count" required>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">

                            <div class="col-md">
                                <div class="form-group ">
                                    <label for="description">Comprobante de pago </label>
                                    <input type="file" class="form-control" name="file" id="file" required>
                                    <input type="hidden" class="form-control" name="filedata" id="filedata" >
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="btn_addTicket" class="btn btn-primary">Procesar Venta</button>
                </div>
            </div>
        </div>
    </div>

    <!-- fin Modal agregar ticket -->
@endsection
