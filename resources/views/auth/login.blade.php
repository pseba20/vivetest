@extends('layouts.simple')

@section('content')
  <div class="" style="background-color: #111433f0;">
    <div class="row mx-0 justify-content-center">
      <div class="hero-static col-lg-6 col-xl-4">
        <div class="content content-full overflow-hidden">
          <!-- Header -->
          <div class="py-30 text-center">
            <a class=" font-w700" href="#">
              {{-- <i class="si si-fire"></i>
                            <span class="font-size-xl text-primary-dark">code</span><span class="font-size-xl">base</span> --}}
              <img src="{{ asset('media/img/logo-minicall.png') }}" alt="" width="200" style="margin-top: -30px">
            </a>
            {{-- <h1 class="h4 font-w700 mt-30 mb-10">Bienvenidos a {{ config('app.name') }}</h1>
                        <h2 class="h5 font-w400 text-muted mb-0">Hoy es un gran día!</h2> --}}
          </div>
          <!-- END Header -->

          <!-- Sign In Form -->
          <!-- jQuery Validation functionality is initialized with .js-validation-signin class in js/pages/op_auth_signin.min.js which was auto compiled from _js/pages/op_auth_signin.js -->
          <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
          <form class="js-validation-signin" action="{{ route('login') }}" method="post">
            @csrf
            <div class="block block-themed block-rounded block-shadow">
              {{-- <div class="block-header" style="background-color: #4FF3C4;">
                                <h3 class="block-title" style="color: black">Ingrese sus credenciales</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option">
                                        <i class="si si-wrench"></i>
                                    </button>
                                </div> --}}
              <div class="block-content">
                <div class="form-group row">
                  <div class="col-12">
                    <label for="login-username">Usuario o Correo electrónico</label>
                    {{-- <input type="text" class="form-control" id="email" name="email"> --}}
                    <input id="login" type="text"
                           class="form-control @if ($errors->has('username') || $errors->has('email')) is-invalid @endif" name="login"
                           value="{{ old('username') ?: old('email') }}" required autocomplete="login" autofocus>
                    @if ($errors->has('username') || $errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-12">
                    <label for="login-password">Contraseña</label>
                    <input type="password" class="form-control" id="password" name="password">
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <div class="col-sm-6 d-sm-flex align-items-center push">
                    {{-- <div class="custom-control custom-checkbox mr-auto ml-0 mb-0">
                                            <input type="checkbox" class="custom-control-input" id="login-remember-me" name="login-remember-me">
                                            <label class="custom-control-label" for="login-remember-me">Remember Me</label>
                                        </div> --}}
                  </div>
                  <div class="col-sm-12 text-sm-center ">
                    <button type="submit" class="btn btn-alt-secondary mb-3" >
                      <i class="si si-login mr-10"></i> Entrar
                    </button>
                  </div>
                </div>
              </div>
               
            </div>
          </form>
          <!-- END Sign In Form -->
        </div>
      </div>
    </div>
  </div>
@endsection
