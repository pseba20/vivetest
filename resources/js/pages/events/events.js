
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btn_newEvent').on('click', function () {
      //alert('here');
      $('.event').modal('show');
      $('#staticBackdropEventLabel').html('Crear evento');
      $('#btn_addEvent').html("Crear");
      $('#btn-save').val("create-event");
      $('#form_event').trigger("reset");
    })

    /** funcion que guarda los datos del nuevo grupo cuando presiona el boton guardar */
    $('#btn_addEvent').on('click', function () {
      $('#btn_addEvent').html("Guardando...");


    // validar campos
      // name
      if ($("#name").val().trim() == ""){
        Swal.fire("Nombre", "Ingrese un nombre válido", "error")
        return;
      }
      // description
      if ($("#description").val().trim() == ""){
        Swal.fire("Descripción", "Ingrese una descripción válida", "error")
        return;
      }

      // count
      if ( parseInt($("#count").val()) == 0 || isNaN(parseInt($("#count").val()))){
        Swal.fire("Cantidad", "Ingrese un cantidad válida", "error")
        return;
      }
      // date
      if ($("#date").val().trim() == ""){
        Swal.fire("Fecha del evento", "Ingrese una fecha de para el evento válida", "error")
        return;
      }
    //
      $.ajax({
          type: "post",
          data: $('#form_event').serialize(),
          url: "/events",
          dataType: 'json',
          success: function (data) {
              $('.event').modal('hide');
              table.draw();
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn_addEvent').html('Error al Guardar');
          }
      });
    });


    /* Abre la ventana editar y carga los datos en los campos */
    $('body').on('click', '.btn_editEvent', function () {
        let event_id = $(this).data('id');
        $.ajax({
            type: "get",
            url: "/events/" + event_id + "/edit",
            success: function (data) {

                console.info('data:', data);
                $('#btn_addEvent').html("Actualizar");
                $('#form_event').trigger("reset");
                $('#staticBackdropEventLabel').html('Editar evento');

                $('.event').modal('show');
                $('#eventId').val(data.id);
                $('#name').val(data.name);
                $('#date').val(data.date);
                $('#count').val(data.count);
                $('#description').val(data.description);


            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    /** Cuando hace click en el boton eliminar team */
    $('body').on('click', '.btn_deleteEvent', function (e) {
      var event_id = $(this).data("id");
      Swal.fire({
          title: 'Eliminar',
          text: "Esta seguro de eliminar a este evento?",
          // type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, borrarlo!'
      }).then((result) => {
          if (result.value) {
              $.ajax({
                  type: "POST",

                  data: { _method: 'delete' },
                  url: "/events/" + event_id,
                  success: function (data) {
                      table.draw();
                      Swal.fire("Eliminado", "Se elimino el evento con exito!", "success")
                      // console.log(data);
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
              });
          }
      })
    });

    var table = $('#eventsDT').DataTable({
      processing: true,
      serverSide: true,
      ajax: "/events",
      language: {
          sProcessing: '<i class="fa fa-3x fa-refresh fa-spin"></i>',
          //     url: './spanish.json',
      },
      columns: [
          { data: 'DT_RowIndex' },
          {
              "render": function (data, type, row) {
                  // console.log(row)
                  return '<span class="text-primary text-uppercase "> ' + row.name + ' </span>';
              }
          },

          {
            "render": function (data, type, row) {
                // console.log(row)
                return '<span class="text-primary text-uppercase "> ' + row.date + ' </span>';
            }
          },

          {
            "render": function (data, type, row) {
              // console.log(row)
              return '<span class="text-primary text-uppercase ">' + row.count + ' </span>';
            }
          },

          {
            "render": function (data, type, row) {
              // console.log(row)
              return '<span class="text-primary text-uppercase "> ' + row.description + ' </span>';
            }
          },

          { data: 'action', orderable: false, searchable: false },
      ]
    });

})
