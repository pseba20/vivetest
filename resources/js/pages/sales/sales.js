
$(document).ready(function () {

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

$('#btn_newTicket').on('click', function () {

$("#event_id").empty();

$.get("/getEvents", function (data) {
  $("#event_id").append(`<option value="0">Ninguno</option>`)
  $.each(data, function (key, val) {
        $("#event_id").append(`<option value="${val.id}">${val.name} (Tickets disponibles : ${val.count}) </option>`)
  })
})

$('.ticket').modal('show');
$('#btn_addTicket').html("Procesar venta");
$('#staticBackdropTicketLabel').html('Vender Tickets');
$('#btn-save').val("create-tickets");
$('#form_ticket').trigger("reset");
})



function getBase64(file) {
var reader = new FileReader();
reader.readAsDataURL(file);
reader.onload = function () {
  $("#filedata").val(reader.result)
};
reader.onerror = function (error) {
  console.log('Error: ', error);
};
}



$("#file").on("change", function(){
var file = document.querySelector('#file').files[0];
getBase64(file); // prints the base64 string
})

/** funcion que guarda los datos del nuevo grupo cuando presiona el boton guardar */
$('#btn_addTicket').on('click', function () {
$('#btn_addTicket').html("Procesando...");


// validar campos
  // evento
  if ($("#event_id").val() == "0"){
    Swal.fire("Evento", "Seleccione un evento válido", "error")
    return;
  }
  // name
  if ($("#name").val().trim() == ""){
    Swal.fire("Nombre", "Ingrese un nombre válido", "error")
    return;
  }
  // surname
  if ($("#surname").val().trim() == ""){
    Swal.fire("Apellido", "Ingrese un apellido válido", "error")
    return;
  }
  // nif/dni
  if ($("#nif").val().trim() == ""){
    Swal.fire("NIF/DNI", "Ingrese un NIF/DNI válido", "error")
    return;
  }
  // count
  if ( parseInt($("#count").val()) == 0 || isNaN(parseInt($("#count").val()))){
    Swal.fire("Cantidad", "Ingrese un cantidad válida", "error")
    return;
  }
  // photo
  if ($("#filedata").val().trim() == ""){
    Swal.fire("Comprobante de pago", "Adjunte un comprobante de pago válido", "error")
    return;
  }
//

$.ajax({
    type: "post",
    data: $('#form_ticket').serialize(),
    url: "/tickets",
    dataType: 'json',
    success: function (data) {

        console.log(data);

        if(data.response){
          $('.ticket').modal('hide');
          table.draw();
        }else{
          Swal.fire(data.responseTitle, data.responseError, "error")
        }
    },
    error: function (data) {
        console.log('Error:', data);
        $('#btn_addTicket').html('Error al Guardar');
    }
});
});


/* Abre la ventana editar y carga los datos en los campos */
$('body').on('click', '.btn_editTicket', function () {
  let event_id = $(this).data('id');
  $.ajax({
      type: "get",
      url: "/tickets/" + event_id + "/edit",
      success: function (data) {

          console.info('data:', data);
          $('#btn_addTicket').html("Actualizar");
          $('#form_ticket').trigger("reset");
          $('#staticBackdropTicketLabel').html('Editar ticket');

          $('.ticket').modal('show');
          $('#ticketId').val(data.id);
          $('#name').val(data.name);
          $('#date').val(data.date);
          $('#count').val(data.count);
          $('#description').val(data.description);


      },
      error: function (data) {
          console.log('Error:', data);
      }
  });
});

/** Cuando hace click en el boton eliminar team */
$('body').on('click', '.btn_deleteTicket', function (e) {
var event_id = $(this).data("id");
Swal.fire({
    title: 'Eliminar',
    text: "Esta seguro de eliminar a este ticket?",
    // type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, borrarlo!'
}).then((result) => {
    if (result.value) {
        $.ajax({
            type: "POST",

            data: { _method: 'delete' },
            url: "/tickets/" + event_id,
            success: function (data) {
                table.draw();
                Swal.fire("Eliminado", "Se elimino el ticket con exito!", "success")
                // console.log(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }
})
});

$('body').on('click', '#thumb', function (e) {



  var w = window.open('about:blank');

// FireFox seems to require a setTimeout for this to work.
setTimeout(() => {
  w.document.body.appendChild(w.document.createElement('iframe')).src =e.target.src;
  w.document.body.style.margin = 0;
  w.document.getElementsByTagName("iframe")[0].style.width = '100%';
  w.document.getElementsByTagName("iframe")[0].style.height = '100%';
  w.document.getElementsByTagName("iframe")[0].style.border = 0;
}, 0);

})

var table = $('#salesDT').DataTable({
processing: true,
serverSide: true,
ajax: {
  url: "/sales",
  type: 'GET',
  data: function (d) {
    d.search = $('input[type="search"]').val();
  }
},


language: {
    sProcessing: '<i class="fa fa-3x fa-refresh fa-spin"></i>',
    //     url: './spanish.json',
},
columns: [
    { data: 'DT_RowIndex' },

    {
        "render": function (data, type, row) {
            // console.log(row)
            return '<span class="text-primary text-uppercase ">' + row.order + ' </span>';
        }
    },


    {
      "render": function (data, type, row) {
          return '<span class="text-primary text-uppercase ">' + row.events.name + ' </span>';
      }
    },

    {
      "render": function (data, type, row) {
          return '<span class="text-primary text-uppercase ">' + row.count + ' </span>';
      }
    },

    {
      "render": function (data, type, row) {
          return '<span class="text-primary text-uppercase ">  <img id="thumb" width="200px" src="'  + row.photo + '">  </span>';
      }
    },

    {
      "render": function (data, type, row) {
          return '<span class="text-primary text-uppercase "> '+ row.name  + ', '+ row.surname + ', ' + row.NIF +'  </span>';
      }
    },

    { data: 'action', orderable: false, searchable: false },
]
});

})
