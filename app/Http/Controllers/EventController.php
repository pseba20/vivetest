<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Event;

class EventController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $events = Event::all();
            return DataTables::of($events)
            ->addIndexColumn()

            ->addColumn('action',  function ($event) {

            $btn = '<button  id="edit-event"    data-id="' . $event->id . '" class="btn btn-secondary btn-sm btn_editEvent"> <i class="fas fa-pencil-alt text-primary"></i> </button>
                    <button  id="delete-event"  data-id="' . $event->id . '" class="btn btn-secondary btn-sm btn_deleteEvent"><i class="fas fa-trash-alt text-danger"></i></button>
                    ';


                return $btn; //($event->event_type_id == 3) ? $con_predictivo : $sinPredictivo;
            })->make(true);
    }
    return view('pages.tickets.tickets');
    }


    public function getEvents()
    {
        $event = Event::where('count', '>', 0)->get();
        return response()->json($event);
    }

    public function store(Request $request)
    {

        $event = Event::updateOrCreate(
            ['id' => $request->eventId],
            [
                'name'              => $request->name,
                'date'              => $request->date,
                'count'            =>  $request->count,
                'description'       => $request->description,
            ]
        );
        return response()->json(['success' => $event]);
    }

    public function edit($id)
    {
        $event = Event::find($id);
        return response()->json($event);
    }

    public function destroy($id)
    {
        $event = Event::find($id)->delete();
        return response()->json(['message' => 'Evento eliminado!']);
    }
}
