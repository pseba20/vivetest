<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Ticket;
use App\Models\Event;
use Illuminate\Support\Str;

class SaleController extends Controller
{
    public function index(Request $request)
    {

        if (request()->ajax()) {
            if (!empty($request->get('search'))) {

                // busqueda por numero de orden o nombre del evento
                $tickets = Ticket::with(['events'])->whereHas('events', function($query) use ($request) {
                      $query->where('name', 'like', "%{$request->get('search')}%")
                      ->orWhere('order', 'like', "%{$request->get('search')}%");

                })->get();

            }
            else {

                $tickets = Ticket::with(['events'])->get();
            }
            return DataTables::of($tickets)
                ->addIndexColumn()


                ->addColumn('action',  function ($ticket) {
                    $btn =  '<button  id="delete-sale"  data-id="' . $ticket->id . '" class="btn btn-secondary btn-sm btn_deleteSale"><i class="fas fa-trash-alt text-danger"></i></button>';
                    return $btn;
                })


                ->make(true);
        }
        return view('pages.sales.sales');
    }


    public function store(Request $request)
    {

        $event = Event::find($request->event_id);
        if($event->count < $request->count)
        {
            return response()->json(['response' => false, 'responseTitle' => 'Cantidad insuficiente', 'responseError' => 'Capacidad disponible superada' ]);
        }

        $latestOrder = Ticket::count();
        $order = '#'.str_pad($latestOrder   + 1, 8, "0", STR_PAD_LEFT);

        $ticket = Ticket::updateOrCreate(
            ['id' => $request->ticketId],
            [
                'order'       => $order,
                'count'       => $request->count,
                'name'        => $request->name,
                'surname'     => $request->surname,
                'NIF'         => $request->nif,
                'photo'       => $request->filedata,
                'event_id'    => $request->event_id,
            ]
        );

        $event = Event::find($request->event_id);
        $event->count = $event->count - $request->count;

        $event->save();

        return response()->json(['response' => true, 'ticket'=> $ticket]);
    }

    public function edit($id)
    {
        $event = Ticket::find($id);
        return response()->json($event);
    }

    public function destroy($id)
    {
        $event = Ticket::find($id)->delete();
        return response()->json(['message' => 'Ticket eliminado!']);
    }
}
