<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    // /**
    //  * Login username to be used by the controller.
    //  *
    //  * @var string
    //  */
    // protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // /**
    //  * Get the login username to be used by the controller.
    //  *
    //  * @return string
    //  */
    // public function findUsername()
    // {
    //     $login = request()->input('login');

    //     $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

    //     request()->merge([$fieldType => $login]);

    //     return $fieldType;
    // }

    // /**
    //  * Get username property.
    //  *
    //  * @return string
    //  */
    public function username()
    {
        $loginData = request()->input('login');
        $fieldName = filter_var($loginData, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $loginData]);
        return $fieldName;
    }

    // function authenticated($user)
    // {
    //     // \LogActivity::addToLog();
    //     $user->online = 1;
    //     $user->campaign_id = null;
    //     $user->save();
    // }
}
