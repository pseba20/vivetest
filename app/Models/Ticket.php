<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable = ['order','count', 'photo', 'name', 'surname', 'NIF', 'event_id'];

    public function events()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }


}
