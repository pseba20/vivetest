<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreEventTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_post_event_without_login(): void
    {
        $response = $this->post('/events');
        $response->assertStatus(302);
    }

    public function test_get_eventsData(): void
    {
        $response = $this->get('/getEvents');
        $response->assertStatus(302);
    }
}
